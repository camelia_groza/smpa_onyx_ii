package com.example.camelia.aac;

/**
 * Created by camelia on 6/14/15.
 */

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class OnyxDataSource {

    // Database fields
    private SQLiteDatabase database;
    private OnyxSQLHelper dbHelper;
    private String[] projection = {
            OnyxSQLHelper._ID,
            OnyxSQLHelper.COLUMN_NAME_TIME,
            OnyxSQLHelper.COLUMN_NAME_PULSE_RATE,
            OnyxSQLHelper.COLUMN_NAME_OXYGEN
    };

    public OnyxDataSource(Context context) {
        dbHelper = new OnyxSQLHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public void addEntry(int pulse, int o2) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();

        ContentValues values = new ContentValues();
        values.put(OnyxSQLHelper.COLUMN_NAME_TIME, dateFormat.format(date));
        values.put(OnyxSQLHelper.COLUMN_NAME_PULSE_RATE, pulse);
        values.put(OnyxSQLHelper.COLUMN_NAME_OXYGEN, o2);

        database.insert(OnyxSQLHelper.TABLE_NAME, null, values);
    }

    public List<Onyx> getAllEntries() {
        List<Onyx> entries = new ArrayList<Onyx>();

        Cursor cursor = database.query(OnyxSQLHelper.TABLE_NAME,
                projection, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Onyx data = cursorToOnyx(cursor);
            entries.add(data);
            System.out.println(data.getTimestamp());
            System.out.println(data.getOxygen());
            System.out.println(data.getPulseRate());
            cursor.moveToNext();
        }

        cursor.close();
        return entries;
    }

    private Onyx cursorToOnyx(Cursor cursor) {
        Onyx data = new Onyx();
        data.setId(cursor.getLong(0));
        data.setTimestamp(cursor.getString(1));
        data.setPulseRate(cursor.getInt(2));
        data.setOxygen(cursor.getInt(3));
        return data;
    }
}
