package com.example.camelia.aac;

import java.util.Date;

/**
 * Created by camelia on 6/14/15.
 */
public class Onyx {
    private long id;
    private int pulse;
    private int o2;
    private String date;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getPulseRate() {
        return pulse;
    }

    public void setPulseRate(int pulse) {
        this.pulse = pulse;
    }

    public String getTimestamp() {
        return date;
    }

    public void setTimestamp(String date) {
        this.date = date;
    }

    public int getOxygen() {
        return o2;
    }

    public void setOxygen(int o2) {
        this.o2 = o2;
    }
}