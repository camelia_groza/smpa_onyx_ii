package com.example.camelia.aac;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by camelia on 6/14/15.
 */
public class OnyxSQLHelper extends SQLiteOpenHelper implements BaseColumns {

    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "Onyx.db";
    public static final String TABLE_NAME = "onyx_data";
    public static final String COLUMN_NAME_TIME = "timestamp";
    public static final String COLUMN_NAME_PULSE_RATE = "pulse";
    public static final String COLUMN_NAME_OXYGEN = "o2";

    public static final String TIME_TYPE = " TIMESTAMP";
    public static final String NR_TYPE = " NUMBER";
    public static final String COMMA_SEP = ",";
    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    _ID + " INTEGER PRIMARY KEY," +
                    COLUMN_NAME_TIME + TIME_TYPE + COMMA_SEP +
                    COLUMN_NAME_PULSE_RATE + NR_TYPE + COMMA_SEP +
                    COLUMN_NAME_OXYGEN + NR_TYPE +
                    " )";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TABLE_NAME;

    public OnyxSQLHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}